package org.blagnac.mpa.quizz.business.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.model.Quizz;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for Quizz
 * 
 * @author Thibault GOUBIN
 */
public class QuizzDao extends AbstractDao<Quizz> {
	
	/**
	 * Singleton instance
	 */
	private static final QuizzDao INSTANCE = new QuizzDao();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuizzDao() {
	}

	@Override
	public List<Quizz> findAll() throws IOException {
		return findAll(new TypeReference<List<Quizz>>() {
		});
	}

	@Override
	public String dataFileName() {
		return DataStorageConstants.QUIZZES_FILE;
	}

	/**
	 * Finds the quizzes containing a question
	 * 
	 * @param questionId the question id
	 * @return the quizzes containing the question
	 * @throws IOException an IO exception
	 */
	public List<Quizz> findContainingQuestion(Integer questionId) throws IOException {
		List<Quizz> quizzes = findAll();
		List<Quizz> quizzesContainingQuestion = new ArrayList<>();

		for (Quizz quizz : quizzes) {
			if (quizz.getQuestions().stream().filter(q -> q.getId() == questionId).count() > 0) {
				quizzesContainingQuestion.add(quizz);
			}
		}

		System.out.println(quizzesContainingQuestion.size() + " quizzes contain question with id '" + questionId + "'");
		return quizzesContainingQuestion;
	}

	public static QuizzDao getInstance() {
		return INSTANCE;
	}
}
