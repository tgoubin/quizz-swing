package org.blagnac.mpa.quizz.business.dao;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.model.PlayerPlayQuizz;
import org.blagnac.mpa.quizz.business.utils.FileUtils;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for PlayerPlayQuizz
 * 
 * @author Thibault GOUBIN
 */
public class PlayerPlayQuizzDao {

	/**
	 * Singleton instance
	 */
	private static final PlayerPlayQuizzDao INSTANCE = new PlayerPlayQuizzDao();

	/**
	 * Results data file path
	 */
	private static final String RESULTS_DATA_FILE_PATH = DataStorageConstants.DATA_STORAGE_PATH
			+ DataStorageConstants.RESULTS_FILE;

	/**
	 * Returns all the PlayerPlayQuizz objects saved in data file
	 * 
	 * @return the PlayerPlayQuizz objects
	 * @throws IOException an IOException
	 */
	public List<PlayerPlayQuizz> findAll() throws IOException {
		return (List<PlayerPlayQuizz>) DataStorageConstants.JSON_MAPPER.readValue(
				FileUtils.getFileContent(RESULTS_DATA_FILE_PATH), new TypeReference<List<PlayerPlayQuizz>>() {
				});
	}

	/**
	 * Adds a PlayerPlayQuizz
	 * 
	 * @param playerPlayQuizz playerPlayQuizz to add
	 * @throws IOException an IO exception
	 */
	public void add(PlayerPlayQuizz playerPlayQuizz) throws IOException {
		List<PlayerPlayQuizz> allResults = findAll();

		allResults.add(playerPlayQuizz);

		FileUtils.writeInFile(RESULTS_DATA_FILE_PATH, DataStorageConstants.JSON_MAPPER.writeValueAsString(allResults),
				true);

		System.out.println("'" + playerPlayQuizz.getClass().getSimpleName() + "' (" + playerPlayQuizz.getPlayerName()
				+ " / " + playerPlayQuizz.getDateTime() + ") object created in '" + RESULTS_DATA_FILE_PATH + "' file");
	}

	public static PlayerPlayQuizzDao getInstance() {
		return INSTANCE;
	}
}
