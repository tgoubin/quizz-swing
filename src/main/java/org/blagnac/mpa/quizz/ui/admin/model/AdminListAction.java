package org.blagnac.mpa.quizz.ui.admin.model;

/**
 * Enumeration of admin lists possible actions
 * 
 * @author Thibault GOUBIN
 */
public enum AdminListAction {

	UPDATE, DELETE;
}
