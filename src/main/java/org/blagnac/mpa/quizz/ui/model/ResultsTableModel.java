package org.blagnac.mpa.quizz.ui.model;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import org.blagnac.mpa.quizz.business.model.PlayerPlayQuizz;

/**
 * Model for results table
 * 
 * @author Thibault GOUBIN
 */
public class ResultsTableModel extends AbstractTableModel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 333282890933483552L;

	/**
	 * Columns
	 */
	private static final String[] COLUMNS = new String[] { "Quizz", "Name", "Date / Time", "Score", "Percentage" };

	private List<PlayerPlayQuizz> results;

	/**
	 * Constructor
	 * 
	 * @param results the results
	 */
	public ResultsTableModel(List<PlayerPlayQuizz> results) {
		this.results = results;
		this.results.sort(new Comparator<PlayerPlayQuizz>() {

			@Override
			public int compare(PlayerPlayQuizz o1, PlayerPlayQuizz o2) {
				int comp = Float.valueOf(o2.getPercentage()).intValue() - Float.valueOf(o1.getPercentage()).intValue();

				if (comp == 0) {
					comp = o2.getNbQuestions() - o1.getNbQuestions();
				}

				return comp;
			}
		});
	}

	@Override
	public int getColumnCount() {
		return COLUMNS.length;
	}

	@Override
	public String getColumnName(int column) {
		return COLUMNS[column];
	}

	@Override
	public int getRowCount() {
		return results.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = null;
		PlayerPlayQuizz result = results.get(rowIndex);

		switch (columnIndex) {
		case 0:
			value = "#" + result.getQuizzId();
			break;
		case 1:
			value = result.getPlayerName();
			break;
		case 2:
			value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(result.getDateTime()));
			break;
		case 3:
			value = result.getScore() + " / " + result.getNbQuestions();
			break;
		case 4:
			value = new DecimalFormat("##0.00").format(result.getPercentage()) + " %";
			break;
		default:
			break;
		}

		return value;
	}

}
