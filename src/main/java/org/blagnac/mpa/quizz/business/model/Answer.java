package org.blagnac.mpa.quizz.business.model;

/**
 * Description class for Answer object
 * 
 * @author Thibault GOUBIN
 */
public class Answer {

	/**
	 * Text for the answer
	 */
	private String text;

	/**
	 * If the answer is correct or not
	 */
	private Boolean correct;

	/**
	 * Constructor
	 */
	public Answer() {
		correct = false;
	}

	/**
	 * Constructor
	 * 
	 * @param text    the text
	 * @param correct if the answer is correct or not
	 */
	public Answer(String text, boolean correct) {
		this.text = text;
		this.correct = correct;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Boolean isCorrect() {
		return correct;
	}

	public void setCorrect(Boolean correct) {
		this.correct = correct;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((correct == null) ? 0 : correct.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Answer other = (Answer) obj;
		if (correct == null) {
			if (other.correct != null)
				return false;
		} else if (!correct.equals(other.correct))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
}
