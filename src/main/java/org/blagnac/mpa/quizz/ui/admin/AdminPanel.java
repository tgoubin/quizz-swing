package org.blagnac.mpa.quizz.ui.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.blagnac.mpa.quizz.ui.BackToHomeButton;
import org.blagnac.mpa.quizz.ui.model.Topic;

/**
 * The administration panel
 * 
 * @author Thibault GOUBIN
 */
public class AdminPanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1599166837512825669L;

	/**
	 * The lists panel
	 */
	private AdminListsPanel listsPanel;

	/**
	 * Constructor
	 */
	public AdminPanel() {
		setLayout(new BorderLayout());

		JPanel infoPanel = new JPanel(new GridLayout(2, 1));
		JPanel btBackToHomePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btBackToHomePanel.add(new BackToHomeButton());
		infoPanel.add(btBackToHomePanel);
		JLabel infoLabel = new JLabel("Double-click on list items to display possible actions");
		infoLabel.setForeground(Color.RED);
		JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		labelPanel.add(infoLabel);
		infoPanel.add(labelPanel);
		add(infoPanel, BorderLayout.NORTH);

		listsPanel = new AdminListsPanel(this);
		add(listsPanel, BorderLayout.SOUTH);
	}

	/**
	 * Displays the admin edit data file panel
	 * 
	 * @param topic    the topic
	 * @param dataFile the data file to edit
	 */
	public void displayAdminEditDataFilePanel(Topic topic, String dataFile) {
		add(new AdminEditDataFilePanel(topic, dataFile, listsPanel), BorderLayout.CENTER);
		validate();
	}
}
