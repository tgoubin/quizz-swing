package org.blagnac.mpa.quizz.business.model;

/**
 * Categories for a question
 * 
 * @author Thibault GOUBIN
 */
public enum QuestionCategory {

	SPORT, MUSIC, GEOGRAPHY, HISTORY, LITERATURE, SCIENCES;
}
