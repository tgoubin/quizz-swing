package org.blagnac.mpa.quizz.ui.model;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.blagnac.mpa.quizz.business.model.AbstractEntity;
import org.blagnac.mpa.quizz.business.service.AbstractService;
import org.blagnac.mpa.quizz.ui.admin.action.AbstractEntityActionProcessor;

/**
 * Enumeration for topics
 * 
 * @author Thibault GOUBIN
 */
public enum Topic {

	QUESTIONS("Question"), QUIZZES("Quizz");

	/**
	 * Label
	 */
	private final String label;

	/**
	 * Constructor
	 * 
	 * @param label the label
	 */
	private Topic(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	/**
	 * Finds the topic from a class name
	 * 
	 * @param clazz the class
	 * @return the topic
	 */
	public static Topic fromClass(Class<? extends AbstractEntity> clazz) {
		return Arrays.asList(Topic.values()).stream().filter(t -> t.getLabel().equals(clazz.getSimpleName()))
				.findFirst().get();
	}

	/**
	 * Calculate and returns the associated service
	 * 
	 * @return the associated service
	 * @throws ClassNotFoundException    a ClassNotFoundException
	 * @throws NoSuchMethodException     a NoSuchMethodException
	 * @throws SecurityException         a SecurityException
	 * @throws IllegalAccessException    an IllegalAccessException
	 * @throws IllegalArgumentException  an IllegalArgumentException
	 * @throws InvocationTargetException an InvocationTargetException
	 */
	public AbstractService getService() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> serviceClass = Class.forName("org.blagnac.mpa.quizz.business.service." + label + "Service");
		Method getInstanceMethod = serviceClass.getDeclaredMethod("getInstance");
		return (AbstractService) getInstanceMethod.invoke(null);
	}

	/**
	 * Calculate and returns the associated action processor
	 * 
	 * @return the associated action processor
	 * @throws ClassNotFoundException    a ClassNotFoundException
	 * @throws NoSuchMethodException     a NoSuchMethodException
	 * @throws SecurityException         a SecurityException
	 * @throws IllegalAccessException    an IllegalAccessException
	 * @throws IllegalArgumentException  an IllegalArgumentException
	 * @throws InvocationTargetException an InvocationTargetException
	 */
	public AbstractEntityActionProcessor getActionProcessor() throws ClassNotFoundException, NoSuchMethodException,
			SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?> processorClass = Class.forName("org.blagnac.mpa.quizz.ui.admin.action." + label + "ActionProcessor");
		Method getInstanceMethod = processorClass.getDeclaredMethod("getInstance");
		return (AbstractEntityActionProcessor) getInstanceMethod.invoke(null);
	}
}
