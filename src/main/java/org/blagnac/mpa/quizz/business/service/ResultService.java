package org.blagnac.mpa.quizz.business.service;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.business.dao.PlayerPlayQuizzDao;
import org.blagnac.mpa.quizz.business.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.business.model.PlayerPlayQuizz;

/**
 * Business service for results (PlayerPlayQuizz)
 * 
 * @author Thibault GOUBIN
 */
public class ResultService {

	/**
	 * Singleton instance
	 */
	private static final ResultService INSTANCE = new ResultService();

	/**
	 * DAO for PlayerPlayQuizz objects
	 */
	private PlayerPlayQuizzDao playerPlayQuizzDao = PlayerPlayQuizzDao.getInstance();

	/**
	 * Finds all results
	 * 
	 * @return the results list
	 */
	public List<PlayerPlayQuizz> getAllResults() {
		System.out.println("Results listing");

		try {
			return playerPlayQuizzDao.findAll();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Saves a quizz result
	 * 
	 * @param result the result
	 */
	public void saveQuizzResult(PlayerPlayQuizz result) {
		try {
			playerPlayQuizzDao.add(result);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	public static ResultService getInstance() {
		return INSTANCE;
	}
}
