package org.blagnac.mpa.quizz.ui.constant;

/**
 * Constants for UI sizes
 * 
 * @author Thibault GOUBIN
 */
public class SizeConstants {

	/**
	 * Frame width
	 */
	public static final int FRAME_WIDTH = 800;

	/**
	 * Frame height
	 */
	public static final int FRAME_HEIGHT = 600;
}
