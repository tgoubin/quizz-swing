package org.blagnac.mpa.quizz.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.blagnac.mpa.quizz.business.model.Quizz;
import org.blagnac.mpa.quizz.business.service.QuizzService;
import org.blagnac.mpa.quizz.business.utils.SecurityUtils;
import org.blagnac.mpa.quizz.configuration.Configuration;
import org.blagnac.mpa.quizz.ui.admin.AdminPanel;
import org.blagnac.mpa.quizz.ui.player.QuizzPlayer;

/**
 * The home panel
 * 
 * @author Thibault GOUBIN
 */
public class HomePanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -489649346213035115L;

	/**
	 * Player button
	 */
	private JButton btPlayer;

	/**
	 * Administrator button
	 */
	private JButton btAdmin;

	/**
	 * Go to the results table button
	 */
	private JButton btResultsTable;

	/**
	 * Quizz service
	 */
	private QuizzService quizzService = QuizzService.getInstance();

	/**
	 * Constructor
	 */
	public HomePanel() {
		setLayout(new GridLayout(4, 1));

		add(new JPanel());

		JPanel panelChoicePanel = new JPanel();
		btPlayer = new JButton("A player");
		btAdmin = new JButton("An administrator");
		panelChoicePanel.add(new JLabel("Choose your profile:"));
		panelChoicePanel.add(btPlayer);
		panelChoicePanel.add(btAdmin);
		add(panelChoicePanel);

		JPanel panelBtResultsTable = new JPanel();
		btResultsTable = new JButton("See the results table");
		panelBtResultsTable.add(btResultsTable);
		add(panelBtResultsTable);

		onClick_btPlayer();
		onClick_btAdmin();
		onClick_btResultsTable();
	}

	/**
	 * Click management on btPlayer
	 */
	private void onClick_btPlayer() {
		btPlayer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object[] quizzes = quizzService.getAllQuizzes().toArray();
				if (quizzes.length > 0) {
					String playerName = JOptionPane.showInputDialog(getParent(), "What's your name?", null,
							JOptionPane.QUESTION_MESSAGE);

					if (playerName != null) {
						if (!"".equals(playerName.trim())) {
							Quizz quizz = (Quizz) JOptionPane.showInputDialog(getParent(), "Choose a quizz:",
									"Play Quizz", JOptionPane.QUESTION_MESSAGE, null, quizzes, quizzes[0]);

							if (quizz != null) {
								QuizzPlayer.play(quizz, playerName, getParent());
							}

						} else {
							JOptionPane.showMessageDialog(getParent(), "The player name is mandatory", null,
									JOptionPane.ERROR_MESSAGE);
						}
					}
				} else {
					JOptionPane.showMessageDialog(getParent(), "There is no available quizz", null,
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}

	/**
	 * Click management on btAdmin
	 */
	private void onClick_btAdmin() {
		btAdmin.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String password = JOptionPane.showInputDialog(getParent(), "Password?", "Admin password",
						JOptionPane.QUESTION_MESSAGE);

				if (password != null) {
					if (SecurityUtils.encodePassword(password)
							.equals(Configuration.CONFIGURATION.get(Configuration.ADMIN_PASSWORD))) {
						QuizzFrame.getInstance().changeContentPane(new AdminPanel());
					} else {
						JOptionPane.showMessageDialog(getParent(), "Invalid password!", null,
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
	}

	/**
	 * Click management on btResultsTable
	 */
	private void onClick_btResultsTable() {
		btResultsTable.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				QuizzFrame.getInstance().changeContentPane(new ResultsTablePanel());
			}
		});
	}
}
