package org.blagnac.mpa.quizz.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.blagnac.mpa.quizz.business.service.ResultService;
import org.blagnac.mpa.quizz.ui.constant.SizeConstants;
import org.blagnac.mpa.quizz.ui.model.ResultsTableModel;

/**
 * The results table panel
 * 
 * @author Thibault GOUBIN
 */
public class ResultsTablePanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 1143954038026358528L;

	/**
	 * Results table
	 */
	private JTable resultsTable;

	/**
	 * Lists width
	 */
	private static final int TABLE_WIDTH = SizeConstants.FRAME_WIDTH - 50;

	/**
	 * Lists height
	 */
	private static final int TABLE_HEIGHT = SizeConstants.FRAME_HEIGHT - 100;

	/**
	 * Constructor
	 */
	public ResultsTablePanel() {
		setLayout(new BorderLayout());

		JPanel btBackToHomePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		btBackToHomePanel.add(new BackToHomeButton());
		add(btBackToHomePanel, BorderLayout.NORTH);

		resultsTable = new JTable();
		resultsTable.setModel(new ResultsTableModel(ResultService.getInstance().getAllResults()));
		resultsTable.setPreferredScrollableViewportSize(new Dimension(TABLE_WIDTH, TABLE_HEIGHT));
		add(new JScrollPane(resultsTable), BorderLayout.CENTER);
	}
}
