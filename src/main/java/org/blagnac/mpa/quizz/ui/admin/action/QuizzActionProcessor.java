package org.blagnac.mpa.quizz.ui.admin.action;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import org.blagnac.mpa.quizz.business.exception.QuizzException;
import org.blagnac.mpa.quizz.business.model.AbstractEntity;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;
import org.blagnac.mpa.quizz.business.service.QuizzService;
import org.blagnac.mpa.quizz.ui.admin.AdminListsPanel;

/**
 * Quizz actions
 * 
 * @author Thibault GOUBIN
 */
public class QuizzActionProcessor extends AbstractEntityActionProcessor {

	/**
	 * Singleton instance
	 */
	private static QuizzActionProcessor INSTANCE = new QuizzActionProcessor();

	/**
	 * Quizz service
	 */
	private QuizzService quizzService = QuizzService.getInstance();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuizzActionProcessor() {
	}

	@Override
	public void processSaveEntityForm(AdminListsPanel adminListsPanel, AbstractEntity entity, Component parent,
			String title) {
		Quizz quizz = (Quizz) entity;
		List<Question> questions = questionService.getAllQuestions();

		Integer[] possibleNbQuestions = new Integer[questions.size()];
		for (int i = 0; i < possibleNbQuestions.length; i++) {
			possibleNbQuestions[i] = i + 1;
		}

		Integer nbQuestions = (Integer) JOptionPane.showInputDialog(parent, "Number of questions:", title,
				JOptionPane.QUESTION_MESSAGE, null, possibleNbQuestions,
				(quizz != null) ? quizz.getQuestions().size() : 1);

		if (nbQuestions != null) {
			List<Question> questionsQuizz = new ArrayList<>();
			boolean cancel = false;
			for (int j = 0; j < nbQuestions; j++) {
				if (!cancel) {
					Object[] selectableQuestions = questions.stream().filter(q -> !questionsQuizz.contains(q))
							.collect(Collectors.toList()).toArray();
					questionsQuizz.add((Question) JOptionPane.showInputDialog(parent, "Question #" + (j + 1) + ":",
							title, JOptionPane.QUESTION_MESSAGE, null, selectableQuestions,
							(quizz != null && quizz.getQuestions().size() > j) ? quizz.getQuestions().get(j).getText()
									: ""));

					if (questionsQuizz.get(j) == null) {
						cancel = true;
					}
				}
			}

			if (!cancel) {
				try {
					Quizz quizzToSave = new Quizz(questionsQuizz);
					if (quizz != null) {
						quizzService.updateQuizz(quizz.getId(), quizzToSave);
					} else {
						quizzService.createQuizz(quizzToSave);
					}
					adminListsPanel.displayQuizzesList();
				} catch (QuizzException exc) {
					JOptionPane.showMessageDialog(parent, "Error during quizz saving: " + exc.getErrorMessage(), null,
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}

	public static QuizzActionProcessor getInstance() {
		return INSTANCE;
	}
}
