package org.blagnac.mpa.quizz.ui.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.model.AbstractEntity;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;
import org.blagnac.mpa.quizz.business.service.QuestionService;
import org.blagnac.mpa.quizz.business.service.QuizzService;
import org.blagnac.mpa.quizz.ui.admin.action.AbstractEntityActionProcessor;
import org.blagnac.mpa.quizz.ui.admin.action.QuestionActionProcessor;
import org.blagnac.mpa.quizz.ui.admin.action.QuizzActionProcessor;
import org.blagnac.mpa.quizz.ui.admin.model.GenericQuizzListModel;
import org.blagnac.mpa.quizz.ui.constant.SizeConstants;
import org.blagnac.mpa.quizz.ui.model.Topic;

/**
 * The administration lists panel
 * 
 * @author Thibault GOUBIN
 */
public class AdminListsPanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 211125462393313679L;

	/**
	 * Admin panel
	 */
	private AdminPanel adminPanel;

	/**
	 * Quizzes list
	 */
	private JList<String> quizzesList;

	/**
	 * Questions list
	 */
	private JList<String> questionsList;

	/**
	 * Create quizz button
	 */
	private JButton btCreateQuizz;

	/**
	 * Edit quizzes data file
	 */
	private JButton btEditQuizzesDataFile;

	/**
	 * Create question button
	 */
	private JButton btCreateQuestion;

	/**
	 * Edit questions data file
	 */
	private JButton btEditQuestionsDataFile;

	/**
	 * Quizz service
	 */
	private QuizzService quizzService = QuizzService.getInstance();

	/**
	 * Question service
	 */
	private QuestionService questionService = QuestionService.getInstance();

	/**
	 * Lists width
	 */
	private static final int LISTS_WIDTH = (SizeConstants.FRAME_WIDTH / 2) - 50;

	/**
	 * Lists height
	 */
	private static final int LISTS_HEIGHT = SizeConstants.FRAME_HEIGHT - 350;

	/**
	 * Labels height
	 */
	private static final int LABELS_HEIGHT = 20;

	/**
	 * Buttons height
	 */
	private static final int BUTTONS_HEIGHT = 40;

	/**
	 * Constructor
	 * 
	 * @param adminPanel the parent panel
	 */
	public AdminListsPanel(AdminPanel adminPanel) {
		this.adminPanel = adminPanel;

		setLayout(new BorderLayout());

		quizzesList = new JList<String>();
		questionsList = new JList<String>();
		quizzesList.setModel(new GenericQuizzListModel<Quizz>());
		questionsList.setModel(new GenericQuizzListModel<Question>());

		quizzesList.setPreferredSize(new Dimension(LISTS_WIDTH, LISTS_HEIGHT));
		questionsList.setPreferredSize(new Dimension(LISTS_WIDTH, LISTS_HEIGHT));

		JPanel titlesPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JLabel quizzesLabel = new JLabel("AVAILABLE QUIZZES");
		quizzesLabel.setPreferredSize(new Dimension(LISTS_WIDTH, LABELS_HEIGHT));
		quizzesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titlesPanel.add(quizzesLabel);
		JLabel questionsLabel = new JLabel("QUESTIONS CATALOG");
		questionsLabel.setPreferredSize(new Dimension(LISTS_WIDTH, LABELS_HEIGHT));
		questionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titlesPanel.add(questionsLabel);
		add(titlesPanel, BorderLayout.NORTH);

		JPanel buttonsPanel = new JPanel(new BorderLayout());
		JPanel buttonsQuizzPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonsQuizzPanel.setPreferredSize(new Dimension(LISTS_WIDTH, BUTTONS_HEIGHT));
		btCreateQuizz = new JButton("Create a quizz");
		btEditQuizzesDataFile = new JButton("Edit the data file");
		buttonsQuizzPanel.add(btEditQuizzesDataFile);
		buttonsQuizzPanel.add(btCreateQuizz);
		buttonsPanel.add(buttonsQuizzPanel, BorderLayout.WEST);
		JPanel buttonsQuestionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		buttonsQuestionPanel.setPreferredSize(new Dimension(LISTS_WIDTH, BUTTONS_HEIGHT));
		btCreateQuestion = new JButton("Create a question");
		btEditQuestionsDataFile = new JButton("Edit the data file");
		buttonsQuestionPanel.add(btEditQuestionsDataFile);
		buttonsQuestionPanel.add(btCreateQuestion);
		buttonsPanel.add(buttonsQuestionPanel, BorderLayout.EAST);
		add(buttonsPanel, BorderLayout.CENTER);

		JPanel listsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		JScrollPane quizzesSP = new JScrollPane(quizzesList);
		quizzesSP.setPreferredSize(new Dimension(LISTS_WIDTH, LISTS_HEIGHT));
		quizzesSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		listsPanel.add(quizzesSP);

		JScrollPane questionsSP = new JScrollPane(questionsList);
		questionsSP.setPreferredSize(new Dimension(LISTS_WIDTH, LISTS_HEIGHT));
		questionsSP.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		listsPanel.add(questionsSP);

		displayQuizzesList();
		displayQuestionsList();

		add(listsPanel, BorderLayout.SOUTH);

		onClick_listsItems(quizzesList, Topic.QUIZZES);
		onClick_listsItems(questionsList, Topic.QUESTIONS);
		onClick_btEditQuizzesDataFile();
		onClick_btEditQuestionsDataFile();
		onClick_btCreateQuizz();
		onClick_btCreateQuestion();
	}

	/**
	 * Displays the quizzes list
	 */
	public void displayQuizzesList() {
		GenericQuizzListModel<Quizz> listModel = (GenericQuizzListModel<Quizz>) quizzesList.getModel();
		listModel.setData(quizzService.getAllQuizzes());
		quizzesList.updateUI();
	}

	/**
	 * Display the questions list
	 */
	public void displayQuestionsList() {
		GenericQuizzListModel<Question> listModel = (GenericQuizzListModel<Question>) questionsList.getModel();
		listModel.setData(questionService.getAllQuestions());
		questionsList.updateUI();
	}

	/**
	 * Click management on lists items
	 * 
	 * @param list  the list
	 * @param topic the topic
	 */
	@SuppressWarnings("unchecked")
	private void onClick_listsItems(JList<String> list, Topic topic) {
		AdminListsPanel adminListsPanel = this;
		list.addMouseListener(new MouseAdapter() {

			public void mouseClicked(MouseEvent evt) {
				JList<String> list = (JList<String>) evt.getSource();
				if (evt.getClickCount() == 2) {
					AbstractEntityActionProcessor.processUpdateOrDeleteForm(adminListsPanel,
							((GenericQuizzListModel<AbstractEntity>) list.getModel()).getData()
									.get(list.locationToIndex(evt.getPoint())),
							topic, getParent());
				}
			}
		});
	}

	/**
	 * Click management on btEditQuizzesDataFile
	 */
	private void onClick_btEditQuizzesDataFile() {
		btEditQuizzesDataFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminPanel.displayAdminEditDataFilePanel(Topic.QUIZZES, DataStorageConstants.QUIZZES_FILE);
			}
		});
	}

	/**
	 * Click management on btEditQuestionsDataFile
	 */
	private void onClick_btEditQuestionsDataFile() {
		btEditQuestionsDataFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				adminPanel.displayAdminEditDataFilePanel(Topic.QUESTIONS, DataStorageConstants.QUESTIONS_FILE);
			}
		});
	}

	/**
	 * Click management on btCreateQuizz
	 */
	private void onClick_btCreateQuizz() {
		AdminListsPanel adminListsPanel = this;
		btCreateQuizz.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				QuizzActionProcessor.getInstance().processSaveEntityForm(adminListsPanel, null, getParent(),
						"Add a quizz");
			}
		});
	}

	/**
	 * Click management on btCreateQuestion
	 */
	private void onClick_btCreateQuestion() {
		AdminListsPanel adminListsPanel = this;
		btCreateQuestion.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				QuestionActionProcessor.getInstance().processSaveEntityForm(adminListsPanel, null, getParent(),
						"Add a question");
			}
		});
	}
}
