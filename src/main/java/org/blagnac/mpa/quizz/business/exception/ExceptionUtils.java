package org.blagnac.mpa.quizz.business.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Utilities for exception
 * 
 * @author Thibault GOUBIN
 */
public class ExceptionUtils {

	/**
	 * Throws a FunctionalException if a condition is checked
	 * 
	 * @param badRequestCondition the BadRequest condition
	 * @param errorMessage        the error message
	 */
	public static void throwFunctionalExceptionIf(boolean badRequestCondition, String errorMessage) {
		if (badRequestCondition) {
			System.err.println(errorMessage);
			throw new FunctionalException(errorMessage);
		}
	}

	/**
	 * Gets the stacktrace of an Exception
	 * 
	 * @param throwable the exception
	 * @return the stacktrace
	 */
	public static String getStackTrace(Throwable throwable) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		throwable.printStackTrace(pw);
		return sw.getBuffer().toString();
	}
}
