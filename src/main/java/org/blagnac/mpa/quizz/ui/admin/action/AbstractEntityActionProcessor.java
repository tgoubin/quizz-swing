package org.blagnac.mpa.quizz.ui.admin.action;

import java.awt.Component;
import java.awt.Container;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JOptionPane;

import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.business.model.AbstractEntity;
import org.blagnac.mpa.quizz.business.service.QuestionService;
import org.blagnac.mpa.quizz.ui.admin.AdminListsPanel;
import org.blagnac.mpa.quizz.ui.admin.model.AdminListAction;
import org.blagnac.mpa.quizz.ui.model.Topic;

/**
 * Generic entities actions
 * 
 * @author Thibault GOUBIN
 */
public abstract class AbstractEntityActionProcessor {

	/**
	 * Question service
	 */
	protected QuestionService questionService = QuestionService.getInstance();

	/**
	 * Processes update or delete choice form
	 * 
	 * @param adminListsPanel the admin lists panel
	 * @param entity          the entity to update / delete
	 * @param topic           the topic
	 * @param parent          the parent component
	 */
	public static void processUpdateOrDeleteForm(AdminListsPanel adminListsPanel, AbstractEntity entity, Topic topic,
			Container parent) {

		try {
			AdminListAction updateOrDelete = (AdminListAction) JOptionPane.showInputDialog(parent,
					"Choose the action to perform:", entity.toString(), JOptionPane.QUESTION_MESSAGE, null,
					AdminListAction.values(), AdminListAction.values()[0]);

			if (updateOrDelete != null) {
				switch (updateOrDelete) {
				case UPDATE:
					topic.getActionProcessor().processSaveEntityForm(adminListsPanel, entity, parent,
							"Update " + entity.getClass().getSimpleName() + " '" + entity.toString() + "'");
					break;
				case DELETE:
					topic.getActionProcessor().processDeleteEntityForm(adminListsPanel, entity, parent);
					break;
				default:
					break;
				}
			}
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			JOptionPane.showMessageDialog(parent, "Error during entity update or delete: " + e.getMessage(), null,
					JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Processes delete entity form
	 * 
	 * @param adminListsPanel the admin lists panel
	 * @param entity          the entity to delete
	 * @param parent          the parent component
	 */
	public void processDeleteEntityForm(AdminListsPanel adminListsPanel, AbstractEntity entity, Component parent) {
		if (JOptionPane.showConfirmDialog(parent,
				"Do you really want to delete this item?") == JOptionPane.YES_OPTION) {
			try {
				Topic.fromClass(entity.getClass()).getService().deleteEntity(entity.getId());
				adminListsPanel.displayQuizzesList();
				adminListsPanel.displayQuestionsList();
			} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				JOptionPane.showMessageDialog(parent, "Error during entity deleting: " + e.getMessage(), null,
						JOptionPane.ERROR_MESSAGE);
			} catch (TechnicalException e) {
				JOptionPane.showMessageDialog(parent, "Error during entity deleting: " + e.getErrorMessage(), null,
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	/**
	 * Processes save quizz form
	 * 
	 * @param adminListsPanel the admin lists panel
	 * @param entity          the entity
	 * @param parent          the parent component
	 * @param title           the form title
	 */
	public abstract void processSaveEntityForm(AdminListsPanel adminListsPanel, AbstractEntity entity, Component parent,
			String title);
}
