package org.blagnac.mpa.quizz.ui.admin.action;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.blagnac.mpa.quizz.business.exception.QuizzException;
import org.blagnac.mpa.quizz.business.model.AbstractEntity;
import org.blagnac.mpa.quizz.business.model.Answer;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.QuestionCategory;
import org.blagnac.mpa.quizz.business.model.QuestionDifficulty;
import org.blagnac.mpa.quizz.ui.admin.AdminListsPanel;

/**
 * Question actions
 * 
 * @author Thibault GOUBIN
 */
public class QuestionActionProcessor extends AbstractEntityActionProcessor {

	/**
	 * Singleton instance
	 */
	private static QuestionActionProcessor INSTANCE = new QuestionActionProcessor();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuestionActionProcessor() {
	}

	@Override
	public void processSaveEntityForm(AdminListsPanel adminListsPanel, AbstractEntity entity, Component parent,
			String title) {
		Question question = (Question) entity;

		String text = (String) JOptionPane.showInputDialog(parent, "Text:", title, JOptionPane.QUESTION_MESSAGE, null,
				null, (question != null) ? question.getText() : "");

		if (text != null) {
			QuestionCategory category = (QuestionCategory) JOptionPane.showInputDialog(parent, "Category:", title,
					JOptionPane.QUESTION_MESSAGE, null, QuestionCategory.values(),
					(question != null) ? question.getCategory() : QuestionCategory.values()[0]);

			if (category != null) {
				QuestionDifficulty difficulty = (QuestionDifficulty) JOptionPane.showInputDialog(parent, "Difficulty:",
						title, JOptionPane.QUESTION_MESSAGE, null, QuestionDifficulty.values(),
						(question != null) ? question.getDifficulty() : QuestionDifficulty.values()[0]);

				if (difficulty != null) {
					Integer nbAnswers = (Integer) JOptionPane.showInputDialog(parent, "Number of possible answers:",
							title, JOptionPane.QUESTION_MESSAGE, null, new Integer[] { 2, 3, 4, 5, 6 },
							(question != null) ? question.getAnswers().size() : 2);

					if (nbAnswers != null) {
						String[] possibleAnswers = new String[nbAnswers];
						boolean cancel = false;
						for (int i = 0; i < nbAnswers; i++) {
							if (!cancel) {
								possibleAnswers[i] = (String) JOptionPane.showInputDialog(parent,
										"Answer #" + (i + 1) + ":", title, JOptionPane.QUESTION_MESSAGE, null, null,
										(question != null && question.getAnswers().size() > i)
												? question.getAnswers().get(i).getText()
												: "");

								if (possibleAnswers[i] == null) {
									cancel = true;
								}
							}
						}

						if (!cancel) {
							Integer[] possibleAnswersIndexes = new Integer[nbAnswers];
							for (int j = 1; j <= nbAnswers; j++) {
								possibleAnswersIndexes[j - 1] = j;
							}
							Integer correctAnswerIndex = (Integer) JOptionPane
									.showInputDialog(parent, "Correct answer:", title, JOptionPane.QUESTION_MESSAGE,
											null, possibleAnswersIndexes,
											(question != null)
													? question.getAnswers()
															.indexOf(question.getAnswers().stream()
																	.filter(a -> a.isCorrect()).findFirst().get())
															+ 1
													: 1);

							if (correctAnswerIndex != null) {
								List<Answer> answers = new ArrayList<>();
								for (int k = 0; k < possibleAnswers.length; k++) {
									answers.add(new Answer(possibleAnswers[k], (k == (correctAnswerIndex - 1))));
								}

								try {
									Question questionToSave = new Question(text.trim(), category, difficulty, answers);
									if (question != null) {
										questionService.updateQuestion(question.getId(), questionToSave);
									} else {
										questionService.createQuestion(questionToSave);
									}
									adminListsPanel.displayQuestionsList();
								} catch (QuizzException exc) {
									JOptionPane.showMessageDialog(parent,
											"Error during question saving: " + exc.getErrorMessage(), null,
											JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					}
				}
			}
		}
	}

	public static QuestionActionProcessor getInstance() {
		return INSTANCE;
	}
}
