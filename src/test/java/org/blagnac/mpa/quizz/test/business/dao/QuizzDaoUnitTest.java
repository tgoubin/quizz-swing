package org.blagnac.mpa.quizz.test.business.dao;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.dao.QuizzDao;
import org.blagnac.mpa.quizz.business.model.Quizz;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for 'QuizzDao'
 * 
 * @author Thibault GOUBIN
 */
public class QuizzDaoUnitTest {

	/**
	 * Data initialization
	 */
	@Before
	public void init() {
		// The data file are in "src/test/resources/data/"
		DataStorageConstants.DATA_STORAGE_PATH = DataStorageConstants.TESTS_DATA_STORAGE_PATH;
	}

	/**
	 * Unit test for 'QuizzDao::findContainingQuestion()' - OK
	 * 
	 * @throws IOException an IO exception
	 */
	@Test
	public void findContainingQuestion_OK() throws IOException {
		System.out.println("Unit test for 'QuizzDao::findContainingQuestion()' - OK");

		QuizzDao quizzDao = QuizzDao.getInstance();

		// Method to test execution
		List<Quizz> quizzes = quizzDao.findContainingQuestion(1);

		// Quizzes ids retrieving
		List<Integer> quizzesIds = quizzes.stream().map(q -> q.getId()).collect(Collectors.toList());

		// I check that 2 quizzes contain the question with id=1
		Assert.assertTrue(quizzes.size() == 2);
		// I check that there is the quizz with id=1
		Assert.assertTrue(quizzesIds.contains(1));
		// I check that there is the quizz with id=2
		Assert.assertTrue(quizzesIds.contains(2));
	}
}
