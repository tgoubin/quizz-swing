package org.blagnac.mpa.quizz.business.model;

/**
 * Difficulties for a question
 * 
 * @author Thibault GOUBIN
 */
public enum QuestionDifficulty {

	EASY(1), MEDIUM(2), HARD(3);

	/**
	 * Notation
	 */
	private final int notation;

	/**
	 * Constructor
	 * 
	 * @param notation the notation
	 */
	private QuestionDifficulty(int notation) {
		this.notation = notation;
	}

	public int getNotation() {
		return notation;
	}
}
