package org.blagnac.mpa.quizz.ui.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.blagnac.mpa.quizz.business.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.ui.constant.SizeConstants;
import org.blagnac.mpa.quizz.ui.model.Topic;

/**
 * The edit data file panel
 * 
 * @author Thibault GOUBIN
 */
public class AdminEditDataFilePanel extends JPanel {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 4194525055912032779L;

	/**
	 * The admin lists panel
	 */
	private AdminListsPanel adminListsPanel;

	/**
	 * Data file input
	 */
	private JTextArea dataFileInput;

	/**
	 * Edit data file button
	 */
	private JButton btEditDataFile;

	/**
	 * The topic
	 */
	private Topic topic;

	/**
	 * Constructor
	 * 
	 * @param topic           the topic
	 * @param dataFile        the data file
	 * @param adminListsPanel the admin lists panel
	 */
	public AdminEditDataFilePanel(Topic topic, String dataFile, AdminListsPanel adminListsPanel) {
		this.topic = topic;
		this.adminListsPanel = adminListsPanel;

		setLayout(new BorderLayout());

		JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JLabel label = new JLabel(topic.getLabel() + " data file content:");
		labelPanel.add(label);
		add(labelPanel, BorderLayout.NORTH);

		JPanel inputPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		dataFileInput = new JTextArea();
		dataFileInput.setPreferredSize(new Dimension(SizeConstants.FRAME_WIDTH - 30, 70));
		dataFileInput.setWrapStyleWord(false);

		try {
			dataFileInput.setText(topic.getService().getDataFileContent());
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			System.err.println(ExceptionUtils.getStackTrace(e));
			dataFileInput.setText("Error during data file content reading");
		}

		inputPanel.add(dataFileInput);
		add(inputPanel, BorderLayout.CENTER);

		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		btEditDataFile = new JButton("Validate");
		buttonPanel.add(btEditDataFile);
		add(buttonPanel, BorderLayout.SOUTH);

		onClick_btEditDataFile();
	}

	/**
	 * Click management on btEditDataFile
	 */
	private void onClick_btEditDataFile() {
		btEditDataFile.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					topic.getService().updateDataFileContent(dataFileInput.getText().trim());
					adminListsPanel.displayQuizzesList();
					adminListsPanel.displayQuestionsList();

					JOptionPane.showMessageDialog(getParent(), "Data file content updated successfully", null,
							JOptionPane.INFORMATION_MESSAGE);
					removeAll();
					getParent().validate();
				} catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException exc) {
					JOptionPane.showMessageDialog(getParent(),
							"Error during data file content updating: " + exc.getMessage(), null,
							JOptionPane.ERROR_MESSAGE);
				} catch (TechnicalException exc) {
					JOptionPane.showMessageDialog(getParent(),
							"Error during data file content updating: " + exc.getErrorMessage(), null,
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}
