package org.blagnac.mpa.quizz.ui.admin.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * Generic list model for Quizz
 * 
 * @author Thibault GOUBIN
 */
public class GenericQuizzListModel<T> extends AbstractListModel<String> {
	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -775802845895268376L;

	/**
	 * Data
	 */
	private List<T> data;

	/**
	 * Constructor
	 */
	public GenericQuizzListModel() {
		data = new ArrayList<>();
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public String getElementAt(int index) {
		return data.get(index).toString();
	}

	@Override
	public int getSize() {
		return data.size();
	}
}
