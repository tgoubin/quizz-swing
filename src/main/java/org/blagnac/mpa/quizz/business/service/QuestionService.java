package org.blagnac.mpa.quizz.business.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.dao.QuestionDao;
import org.blagnac.mpa.quizz.business.dao.QuizzDao;
import org.blagnac.mpa.quizz.business.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.business.exception.FunctionalException;
import org.blagnac.mpa.quizz.business.exception.QuizzException;
import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.business.model.Answer;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Business service for Question
 * 
 * @author Thibault GOUBIN
 */
public class QuestionService extends AbstractService {

	/**
	 * Singleton instance
	 */
	private static final QuestionService INSTANCE = new QuestionService();

	/**
	 * DAO for question objects
	 */
	private QuestionDao questionDao = QuestionDao.getInstance();

	/**
	 * DAO for quizz objects
	 */
	private QuizzDao quizzDao = QuizzDao.getInstance();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuestionService() {
	}

	/**
	 * Finds all questions
	 * 
	 * @return the questions list
	 */
	public List<Question> getAllQuestions() {
		System.out.println("Questions listing");

		try {
			return questionDao.findAll();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Returns a question by a given id
	 * 
	 * @param id the question id
	 * @return the question
	 * @throws TechnicalException a TechnicalException
	 */
	public Question getQuestionById(Integer id) throws TechnicalException {
		try {
			return questionDao.findById(id).get();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Creates a question
	 * 
	 * @param question the question
	 * @return the question created
	 * @throws QuizzException a QuizzException
	 */
	public Question createQuestion(Question question) throws QuizzException {
		System.out.println("Question creation : '" + question.getText() + "'");

		try {
			createOrUpdateCheckQuestionFields(question);

			// Fields cleaning
			question.setText(question.getText().trim());
			for (Answer answer : question.getAnswers()) {
				answer.setText(answer.getText().trim());
			}

			return questionDao.create(question);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Updates a question
	 * 
	 * @param id       the question id
	 * @param question the question
	 * @return the question updated
	 * @throws QuizzException a QuizzException
	 */
	public Question updateQuestion(Integer id, Question question) throws QuizzException {
		System.out.println("Question update : id = '" + id + "'");

		try {
			createOrUpdateCheckQuestionFields(question);

			Question questionToUpdate = getQuestionById(id);

			// Fields updating and cleaning
			questionToUpdate.setCategory(question.getCategory());
			questionToUpdate.setDifficulty(question.getDifficulty());
			questionToUpdate.setText(question.getText().trim());
			for (Answer answer : question.getAnswers()) {
				answer.setText(answer.getText().trim());
			}
			questionToUpdate.setAnswers(question.getAnswers());

			return questionDao.update(id, questionToUpdate);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Question fields checks
	 * 
	 * @param question the question
	 * @throws FunctionalException a FunctionalException
	 */
	private void createOrUpdateCheckQuestionFields(Question question) throws FunctionalException {
		// Mandatory fields
		ExceptionUtils.throwFunctionalExceptionIf(question.getText() == null || "".equals(question.getText().trim()),
				"The question text is mandatory");

		// Answers checking
		ExceptionUtils.throwFunctionalExceptionIf(question.getAnswers().stream()
				.filter(a -> a.getText() == null || "".equals(a.getText().trim())).count() > 0,
				"The answer text is mandatory");
	}

	@Override
	public void deleteEntity(Integer id) throws TechnicalException {
		System.out.println("Question deletion : id = '" + id + "'");

		try {
			// Just for id check
			getQuestionById(id);

			for (Quizz quizz : quizzDao.findContainingQuestion(id)) {
				quizz.getQuestions().removeIf(q -> q.getId() == id);
				quizzDao.update(quizz.getId(), quizz);
			}

			questionDao.delete(id);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public String getDataFileContent() throws TechnicalException {
		try {
			return questionDao.getStorageFileContent();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void updateDataFileContent(String questionsFileContent) throws TechnicalException {
		try {
			// We try to map in object in order to validate the JSON string
			DataStorageConstants.JSON_MAPPER.readValue(questionsFileContent, new TypeReference<List<Question>>() {
			});

			// If OK
			questionDao.updateStorageFileContent(questionsFileContent);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Enriches a question object
	 * 
	 * @param question the question to enrich
	 * @throws IOException an IO exception
	 */
	public void enrichQuestion(Question question) throws IOException {
		Optional<Question> optionalQuestion = questionDao.findById(question.getId());

		if (optionalQuestion.isPresent()) {
			question.setAnswers(optionalQuestion.get().getAnswers());
			question.setCategory(optionalQuestion.get().getCategory());
			question.setDifficulty(optionalQuestion.get().getDifficulty());
			question.setText(optionalQuestion.get().getText());
		}
	}

	/**
	 * Cleans a question object (without details)
	 * 
	 * @param question the question
	 * @throws IOException an IO exception
	 */
	public void cleanQuestion(Question question) throws IOException {
		Optional<Question> optionalQuestion = questionDao.findById(question.getId());

		if (optionalQuestion.isPresent()) {
			question.setAnswers(null);
			question.setCategory(null);
			question.setDifficulty(null);
			question.setText(null);
		}
	}

	public static QuestionService getInstance() {
		return INSTANCE;
	}
}
