package org.blagnac.mpa.quizz.business.dao;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.model.Question;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Data Access Object for Question
 * 
 * @author Thibault GOUBIN
 */
public class QuestionDao extends AbstractDao<Question> {

	/**
	 * Singleton instance
	 */
	private static final QuestionDao INSTANCE = new QuestionDao();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuestionDao() {
	}

	@Override
	public List<Question> findAll() throws IOException {
		return findAll(new TypeReference<List<Question>>() {
		});
	}

	@Override
	public String dataFileName() {
		return DataStorageConstants.QUESTIONS_FILE;
	}

	public static QuestionDao getInstance() {
		return INSTANCE;
	}
}
