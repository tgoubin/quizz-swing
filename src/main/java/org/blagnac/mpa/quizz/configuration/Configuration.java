package org.blagnac.mpa.quizz.configuration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Application configuration
 * 
 * @author Thibault GOUBIN
 */
public class Configuration {

	/**
	 * Configuration file name
	 */
	private static final String CONFIGURATION_FILENAME = "configuration.properties";

	/**
	 * Configuration
	 */
	public static Properties CONFIGURATION = null;

	/**
	 * "admin.password" configuration variable
	 */
	public static final String ADMIN_PASSWORD = "admin.password";

	/**
	 * Configuration loading
	 */
	public static void loadConfiguration() {
		System.out.println("Application configuration loading...");

		try {
			CONFIGURATION = new Properties();
			InputStream is = Configuration.class.getClassLoader().getResourceAsStream(CONFIGURATION_FILENAME);

			if (is != null) {
				CONFIGURATION.load(is);
			} else {
				throw new FileNotFoundException("Configuration file '" + CONFIGURATION_FILENAME + "' does not exist");
			}
		} catch (IOException ioe) {
			System.err.println("Error during configuration file loading");
			ioe.printStackTrace();
		}
	}
}
