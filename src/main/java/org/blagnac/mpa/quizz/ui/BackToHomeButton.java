package org.blagnac.mpa.quizz.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

/**
 * Back to home button
 * 
 * @author Thibault GOUBIN
 */
public class BackToHomeButton extends JButton {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 5419504795179407955L;

	/**
	 * Constructor
	 */
	public BackToHomeButton() {
		super("Back to home");

		addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				QuizzFrame.getInstance().changeContentPane(new HomePanel());
			}
		});
	}
}
