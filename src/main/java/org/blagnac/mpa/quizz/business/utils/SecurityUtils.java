package org.blagnac.mpa.quizz.business.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import org.blagnac.mpa.quizz.business.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.business.exception.TechnicalException;

/**
 * Utilities for security
 * 
 * @author Thibault GOUBIN
 */
public class SecurityUtils {

	/**
	 * Password encoding (MD5 + Base64)
	 * 
	 * @param password the password
	 * @return the password encoded
	 */
	public static String encodePassword(String password) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			byte[] md5InBytes = messageDigest.digest(password.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(md5InBytes);
		} catch (NoSuchAlgorithmException e) {
			throw new TechnicalException("Error during mot de passe encoding", ExceptionUtils.getStackTrace(e));
		}
	}
}
