package org.blagnac.mpa.quizz.business.exception;

/**
 * Class for all exceptions
 * 
 * @author Thibault GOUBIN
 */
public abstract class QuizzException extends RuntimeException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 5952610642963155292L;

	/**
	 * The error message
	 */
	private final String errorMessage;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public QuizzException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}
