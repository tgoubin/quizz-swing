package org.blagnac.mpa.quizz.test.business.utils;

import org.blagnac.mpa.quizz.business.utils.SecurityUtils;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for 'SecurityUtils'
 * 
 * @author Thibault GOUBIN
 */
public class SecurityUtilsUnitTest {

	/**
	 * Unit test for 'SecurityUtils::encodePassword()' - OK
	 */
	@Test
	public void encodePassword_OK() {
		System.out.println("Unit test for 'SecurityUtils::encodePassword()' - OK");

		String expected1 = "ZRLtFfQzNLTwW7Z2Wa1HjQ==";
		String expected2 = "iyxOKImAU24TTstZpye1iA==";

		Assert.assertEquals(expected1, SecurityUtils.encodePassword("mpaproject"));
		Assert.assertEquals(expected2, SecurityUtils.encodePassword("projectmpa"));
	}
}
