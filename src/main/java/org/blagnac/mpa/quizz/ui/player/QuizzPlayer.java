package org.blagnac.mpa.quizz.ui.player;

import java.awt.Component;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;

import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.business.model.PlayerPlayQuizz;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;
import org.blagnac.mpa.quizz.business.service.ResultService;

/**
 * Quizz player
 * 
 * @author Thibault GOUBIN
 */
public class QuizzPlayer {

	/**
	 * Run the quizz
	 * 
	 * @param quizz      the chose quizz
	 * @param playerName the player name
	 * @param parent     the parent component
	 */
	public static void play(Quizz quizz, String playerName, Component parent) {
		boolean cancel = false;
		List<Boolean> playerGoodAnswers = new ArrayList<>();
		for (int i = 0; i < quizz.getQuestions().size(); i++) {
			if (!cancel) {
				Question question = quizz.getQuestions().get(i);
				StringBuilder text = new StringBuilder(question.getText());
				text = text.append("\n\n");

				for (int j = 0; j < question.getAnswers().size(); j++) {
					text = text.append(j + 1).append("- ").append(question.getAnswers().get(j).getText()).append("\n");
				}

				text = text.append("\nChoose an answer:");

				Integer[] possibleAnswers = new Integer[question.getAnswers().size()];
				for (int k = 1; k <= question.getAnswers().size(); k++) {
					possibleAnswers[k - 1] = k;
				}
				Integer answer = (Integer) JOptionPane.showInputDialog(parent, text.toString(), "Question #" + (i + 1),
						JOptionPane.QUESTION_MESSAGE, null, possibleAnswers, possibleAnswers[0]);

				if (answer != null) {
					boolean isGoodAnswer = question.getAnswers().get(answer - 1).isCorrect();
					playerGoodAnswers.add(isGoodAnswer);

					String scoreMessage = playerGoodAnswers.stream().filter(a -> a).count() + " / "
							+ playerGoodAnswers.size();
					if (i < (quizz.getQuestions().size() - 1)) {
						scoreMessage = "\n\nYour score is " + scoreMessage;
					} else {
						scoreMessage = "\n\nThe game is over. Your final score is " + scoreMessage;
					}

					if (isGoodAnswer) {
						JOptionPane.showMessageDialog(parent, "Good answer !!" + scoreMessage, null,
								JOptionPane.INFORMATION_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(parent,
								"The answer is wrong. The good one was '" + question.getAnswers().stream()
										.filter(a -> a.isCorrect()).findFirst().get().getText() + "'" + scoreMessage,
								null, JOptionPane.ERROR_MESSAGE);
					}
				} else {
					cancel = true;
				}
			}
		}

		if (!cancel) {
			try {
				ResultService.getInstance()
						.saveQuizzResult(new PlayerPlayQuizz(quizz.getId(), playerName,
								(int) playerGoodAnswers.stream().filter(a -> a).count(), quizz.getQuestions().size(),
								new Date().getTime()));
			} catch (TechnicalException e) {
				JOptionPane.showMessageDialog(parent, "Error during quizz result saving: " + e.getErrorMessage(), null,
						JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
}
