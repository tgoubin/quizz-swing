package org.blagnac.mpa.quizz.ui;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.blagnac.mpa.quizz.ui.constant.SizeConstants;

/**
 * Quizz main frame
 * 
 * @author Thibault GOUBIN
 */
public class QuizzFrame extends JFrame {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -1298937141119782465L;

	/**
	 * Singleton instance
	 */
	private static final QuizzFrame INSTANCE = new QuizzFrame();

	/**
	 * Constructor
	 */
	private QuizzFrame() {
		setTitle("Quizz game");
		setSize(SizeConstants.FRAME_WIDTH, SizeConstants.FRAME_HEIGHT);
		setLocationRelativeTo(getParent());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel profileChoicePanel = new HomePanel();
		setContentPane(profileChoicePanel);

		setVisible(true);
	}

	/**
	 * Changes the content pane
	 * 
	 * @param panel the panel to put as content pane
	 */
	public void changeContentPane(JPanel panel) {
		setContentPane(panel);
		validate();
	}

	public static QuizzFrame getInstance() {
		return INSTANCE;
	}
}
