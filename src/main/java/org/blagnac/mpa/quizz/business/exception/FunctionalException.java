package org.blagnac.mpa.quizz.business.exception;

/**
 * Function exception
 * 
 * @author Thibault GOUBIN
 */
public class FunctionalException extends QuizzException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = -7034798532912567510L;

	/**
	 * Constructor
	 * 
	 * @param errorMessage the error message
	 */
	public FunctionalException(String errorMessage) {
		super(errorMessage);
	}
}
