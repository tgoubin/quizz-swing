package org.blagnac.mpa.quizz.test.business.service;

import java.io.IOException;
import java.util.Arrays;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.exception.FunctionalException;
import org.blagnac.mpa.quizz.business.model.Answer;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.QuestionCategory;
import org.blagnac.mpa.quizz.business.model.QuestionDifficulty;
import org.blagnac.mpa.quizz.business.service.QuestionService;
import org.blagnac.mpa.quizz.business.utils.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for 'QuestionService'
 * 
 * @author Thibault GOUBIN
 */
public class QuestionServiceUnitTest {

	/**
	 * Data initialization
	 * 
	 * @throws IOException an IOException
	 */
	@Before
	public void init() throws IOException {
		// The data file are in "src/test/resources/data/"
		DataStorageConstants.DATA_STORAGE_PATH = DataStorageConstants.TESTS_DATA_STORAGE_PATH;

		// Reset questions data file
		FileUtils.writeInFile(DataStorageConstants.TESTS_DATA_STORAGE_PATH + DataStorageConstants.QUESTIONS_FILE,
				DataStorageConstants.EMPTY_FILE_CONTENT, true);
	}

	/**
	 * Unit test for 'QuestionService::createQuestion()' - OK
	 */
	@Test
	public void createQuestion_OK() {
		System.out.println("Unit test for 'QuestionService::createQuestion()' - OK");

		QuestionService questionService = QuestionService.getInstance();

		// Question to create
		Question question = new Question("Question to test text", QuestionCategory.HISTORY, QuestionDifficulty.MEDIUM,
				Arrays.asList(new Answer("Answer text 1", false), new Answer("Answer text 2", true),
						new Answer("Answer text 3", false)));

		// Method to test execution
		Question questionCreated = questionService.createQuestion(question);

		// Question created checking
		Assert.assertTrue(questionCreated.getId() == 1);
		Assert.assertEquals(questionCreated.getText(), question.getText());
		Assert.assertEquals(questionCreated.getCategory(), question.getCategory());
		Assert.assertEquals(questionCreated.getDifficulty(), question.getDifficulty());
		Assert.assertTrue(questionCreated.getAnswers().size() == question.getAnswers().size());

		for (Answer answer : question.getAnswers()) {
			Assert.assertTrue(questionCreated.getAnswers().contains(answer));
		}
	}

	/**
	 * Unit test for 'QuestionService::createQuestion()' - KO - Mandatory fields
	 * missing
	 */
	@Test
	public void createQuestion_KO_MandatoryFieldsMissing() {
		System.out.println("Unit test for 'QuestionService::createQuestion()' - KO - Mandatory fields missing");

		QuestionService questionService = QuestionService.getInstance();

		// Method to test executions - checking functional exception is thrown
		Assert.assertThrows(FunctionalException.class, () -> {
			questionService.createQuestion(new Question(" ", QuestionCategory.HISTORY, QuestionDifficulty.MEDIUM,
					Arrays.asList(new Answer("Answer text 1", false), new Answer("Answer text 2", true),
							new Answer("Answer text 3", false))));
		});
		Assert.assertThrows(FunctionalException.class, () -> {
			questionService.createQuestion(new Question("Question to test text", QuestionCategory.HISTORY,
					QuestionDifficulty.MEDIUM, Arrays.asList(new Answer("Answer text 1", false), new Answer("", true),
							new Answer("Answer text 3", false))));
		});
	}
}
