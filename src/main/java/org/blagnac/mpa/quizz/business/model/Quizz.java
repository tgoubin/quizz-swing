package org.blagnac.mpa.quizz.business.model;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

/**
 * Description class for Quizz object
 * 
 * @author Thibault GOUBIN
 */
public class Quizz extends AbstractEntity {

	/**
	 * Questions list
	 */
	private List<Question> questions;

	/**
	 * Constructor
	 */
	public Quizz() {
		questions = new ArrayList<>();
	}

	/**
	 * Constructor
	 * 
	 * @param questions the questions
	 */
	public Quizz(List<Question> questions) {
		this.questions = questions;
	}

	public List<Question> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	@Override
	public String toString() {
		OptionalDouble optionalDifficulty = questions.stream().mapToDouble(Question::getDifficultyNotation).average();
		String difficulty = (optionalDifficulty.isPresent())
				? new DecimalFormat("##0.00").format(optionalDifficulty.getAsDouble())
				: "?";
		return "#" + getId() + " - " + questions.size() + " question(s) - Difficulty : " + difficulty + " / 3";
	}
}
