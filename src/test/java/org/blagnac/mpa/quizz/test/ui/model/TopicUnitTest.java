package org.blagnac.mpa.quizz.test.ui.model;

import java.lang.reflect.InvocationTargetException;

import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;
import org.blagnac.mpa.quizz.business.service.QuestionService;
import org.blagnac.mpa.quizz.business.service.QuizzService;
import org.blagnac.mpa.quizz.ui.admin.action.QuestionActionProcessor;
import org.blagnac.mpa.quizz.ui.admin.action.QuizzActionProcessor;
import org.blagnac.mpa.quizz.ui.model.Topic;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit tests for 'Topic'
 * 
 * @author Thibault GOUBIN
 */
public class TopicUnitTest {

	/**
	 * Unit test for 'Topic::fromClass()' - OK
	 */
	@Test
	public void fromClass_OK() {
		System.out.println("Unit test for 'Topic::fromClass()' - OK");

		Assert.assertTrue(Topic.fromClass(Question.class) == Topic.QUESTIONS);
		Assert.assertTrue(Topic.fromClass(Quizz.class) == Topic.QUIZZES);
	}

	/**
	 * Unit test for 'Topic::getService()' - OK
	 * 
	 * @throws InvocationTargetException an InvocationTargetException
	 * @throws IllegalArgumentException  an IllegalArgumentException
	 * @throws IllegalAccessException    an IllegalAccessException
	 * @throws SecurityException         a SecurityException
	 * @throws NoSuchMethodException     a NoSuchMethodException
	 * @throws ClassNotFoundException    a ClassNotFoundException
	 */
	@Test
	public void getService_OK() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("Unit test for 'Topic::getService()' - OK");

		Assert.assertTrue(Topic.QUESTIONS.getService().getClass() == QuestionService.class);
		Assert.assertTrue(Topic.QUIZZES.getService().getClass() == QuizzService.class);
	}

	/**
	 * Unit test for 'Topic::getActionProcessor()' - OK
	 * 
	 * @throws InvocationTargetException an InvocationTargetException
	 * @throws IllegalArgumentException  an IllegalArgumentException
	 * @throws IllegalAccessException    an IllegalAccessException
	 * @throws SecurityException         a SecurityException
	 * @throws NoSuchMethodException     a NoSuchMethodException
	 * @throws ClassNotFoundException    a ClassNotFoundException
	 */
	@Test
	public void getActionProcessor_OK() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		System.out.println("Unit test for 'Topic::getActionProcessor()' - OK");

		Assert.assertTrue(Topic.QUESTIONS.getActionProcessor().getClass() == QuestionActionProcessor.class);
		Assert.assertTrue(Topic.QUIZZES.getActionProcessor().getClass() == QuizzActionProcessor.class);
	}
}
