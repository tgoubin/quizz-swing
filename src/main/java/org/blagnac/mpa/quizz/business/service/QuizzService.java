package org.blagnac.mpa.quizz.business.service;

import java.io.IOException;
import java.util.List;

import org.blagnac.mpa.quizz.business.constant.DataStorageConstants;
import org.blagnac.mpa.quizz.business.dao.QuizzDao;
import org.blagnac.mpa.quizz.business.exception.ExceptionUtils;
import org.blagnac.mpa.quizz.business.exception.TechnicalException;
import org.blagnac.mpa.quizz.business.model.Question;
import org.blagnac.mpa.quizz.business.model.Quizz;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * Service for Quizz
 * 
 * @author Thibault GOUBIN
 */
public class QuizzService extends AbstractService {

	/**
	 * Singleton instance
	 */
	private static final QuizzService INSTANCE = new QuizzService();

	/**
	 * DAO for quizz objects
	 */
	private QuizzDao quizzDao = QuizzDao.getInstance();

	/**
	 * Service for question objects
	 */
	private QuestionService questionService = QuestionService.getInstance();

	/**
	 * Private constructor to obligate singleton use
	 */
	private QuizzService() {
	}

	/**
	 * Finds all quizzes
	 * 
	 * @return the quizzes list
	 * @throws TechnicalException a TechnicalException
	 */
	public List<Quizz> getAllQuizzes() throws TechnicalException {
		System.out.println("Quizzes listing");

		try {
			List<Quizz> quizzes = quizzDao.findAll();

			for (Quizz quizz : quizzes) {
				enrichQuizz(quizz);
			}

			return quizzes;
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Enriches a quizz object (with questions details)
	 * 
	 * @param quizz the quizz
	 * @throws IOException an IO exception
	 */
	private void enrichQuizz(Quizz quizz) throws IOException {
		if (quizz.getQuestions() != null && !quizz.getQuestions().isEmpty()) {
			for (Question questionInQuizz : quizz.getQuestions()) {
				questionService.enrichQuestion(questionInQuizz);
			}
		}
	}

	/**
	 * Returns a quizz by a given id
	 * 
	 * @param id the quizz id
	 * @return the quizz
	 * @throws TechnicalException a TechnicalException
	 */
	private Quizz getQuizzById(Integer id) throws TechnicalException {
		try {
			return quizzDao.findById(id).get();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Creates a quizz
	 * 
	 * @param quizz the quizz
	 * @return the quizz created
	 * @throws TechnicalException a TechnicalException
	 */
	public Quizz createQuizz(Quizz quizz) throws TechnicalException {
		System.out.println("Quizz creation - " + quizz.getQuestions().size() + " questions");

		try {
			cleanQuizz(quizz);
			return quizzDao.create(quizz);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Updates a quizz
	 * 
	 * @param id    the quizz id
	 * @param quizz the quizz
	 * @return the quizz updated
	 * @throws TechnicalException a TechnicalException
	 */
	public Quizz updateQuizz(Integer id, Quizz quizz) throws TechnicalException {
		System.out.println("Quizz deletion : id = '" + id + "'");

		try {
			Quizz quizzToUpdate = getQuizzById(id);

			// Fields updating
			quizzToUpdate.setQuestions(quizz.getQuestions());

			cleanQuizz(quizzToUpdate);
			return quizzDao.update(id, quizzToUpdate);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	/**
	 * Cleans a quizz object (without questions details)
	 * 
	 * @param quizz the quizz
	 * @throws IOException an IO exception
	 */
	private void cleanQuizz(Quizz quizz) throws IOException {
		if (quizz.getQuestions() != null && !quizz.getQuestions().isEmpty()) {
			for (Question questionInQuizz : quizz.getQuestions()) {
				questionService.cleanQuestion(questionInQuizz);
			}
		}
	}

	@Override
	public void deleteEntity(Integer id) throws TechnicalException {
		System.out.println("Quizz deletion : id = '" + id + "'");

		try {
			// Just for id check
			getQuizzById(id);

			quizzDao.delete(id);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public String getDataFileContent() throws TechnicalException {
		try {
			return quizzDao.getStorageFileContent();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	@Override
	public void updateDataFileContent(String quizzesFileContent) throws TechnicalException {
		try {
			// We try to map in object in order to validate the JSON string
			DataStorageConstants.JSON_MAPPER.readValue(quizzesFileContent, new TypeReference<List<Quizz>>() {
			});

			// If OK
			quizzDao.updateStorageFileContent(quizzesFileContent);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			throw new TechnicalException(e.getMessage(), ExceptionUtils.getStackTrace(e));
		}
	}

	public static QuizzService getInstance() {
		return INSTANCE;
	}
}
