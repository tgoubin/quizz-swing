package org.blagnac.mpa.quizz.business.service;

import org.blagnac.mpa.quizz.business.exception.TechnicalException;

/**
 * Abstract class for services
 * 
 * @author Thibault GOUBIN
 */
public abstract class AbstractService {

	/**
	 * Returns the data file content
	 * 
	 * @return the data file content
	 * @throws TechnicalException a TechnicalException
	 */
	public abstract String getDataFileContent() throws TechnicalException;

	/**
	 * Updates the data file content
	 * 
	 * @param dataFileContent the data file content
	 * @throws TechnicalException a TechnicalException
	 */
	public abstract void updateDataFileContent(String dataFileContent) throws TechnicalException;

	/**
	 * Deletes an entity
	 * 
	 * @param id the entity id
	 * @throws TechnicalException a TechnicalException
	 */
	public abstract void deleteEntity(Integer id) throws TechnicalException;
}
