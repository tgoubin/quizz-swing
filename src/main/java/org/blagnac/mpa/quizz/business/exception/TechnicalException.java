package org.blagnac.mpa.quizz.business.exception;

/**
 * Technical exception
 * 
 * @author Thibault GOUBIN
 */
public class TechnicalException extends QuizzException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 3095484820889220182L;

	/**
	 * Stacktrace of the cause
	 */
	private final String causeStackTrace;

	/**
	 * Constructor
	 * 
	 * @param errorMessage    the error message
	 * @param causeStackTrace the stack trace of the cause
	 */
	public TechnicalException(String errorMessage, String causeStackTrace) {
		super(errorMessage);
		this.causeStackTrace = causeStackTrace;
	}

	public String getCauseStackTrace() {
		return causeStackTrace;
	}
}
